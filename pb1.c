#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct
{
	char denumire[50];
	int nr_aparitii;
	float frecventa;
	int total;
}CUVANT;

CUVANT c[1001];

void read()
{
	FILE *f = NULL;
	if ((f = fopen("random_ex1.csv","r")) == NULL)
	{
		printf("nu exista acest fisier");
		return;
	}
	int i = 0;
	char line[1000];
	if (fgets(line, 1000, f) == NULL)
	{
		printf("nu exista prima linie");
		return;
	}
	char *p;
	while (fgets(line, 1000, f) != NULL && i<1000)
	{
		p = strtok(line, ",");
		strcpy(c[i].denumire, p);
		p = strtok(NULL, ",");
		c[i].nr_aparitii = atoi(p);
		p = strtok(NULL, ",");
		c[i].frecventa = atof(p);
		if (c[i].frecventa)
		{
			c[i].total = c[i].nr_aparitii / c[i].frecventa;
		}
		else
		{
			c[i].total = 0;
		}
		i++;
	}
	if (fclose(f) != 0)
	{
		printf("nu s-a putut inchide fisierul");
		return;
	}
}

void sortare(FILE* f, char** cuv2, int nrcuv2, int sign)
{
	char* temp;
	for (int i = 0; i < nrcuv2; i++)
	{
		for (int j = i + 1; j < nrcuv2; j++)
		{
			if (strcmp(cuv2[i], cuv2[j]) * sign > 0)
			{
				temp = cuv2[i];
				cuv2[i] = cuv2[j];
				cuv2[j] = temp;
			}
		}
	}
}

void determinare_frecventa_nraparitii(FILE* f)
{
	int i, nrcuv2=0;
	char* cuv2[1002];
	double minim_frecventa = 100;
	int maxim_nraparitii = 0; 
	for (i = 0; i < 1000; i++)
	{
		if (c[i].frecventa < minim_frecventa)
		{
			minim_frecventa = c[i].frecventa;
		}
		if(c[i].nr_aparitii > maxim_nraparitii)
		{
			maxim_nraparitii = c[i].nr_aparitii;
		}
	}
	fprintf(f, "Fmin : %.2f\n", minim_frecventa);
	sortare(f, cuv2, nrcuv2, 1);
	fprintf(f, "Amax : %d\n", maxim_nraparitii);
	sortare(f, cuv2, nrcuv2, 1);

	for (i = 0; i < 1000; i++)
	{
		if (c[i].nr_aparitii == maxim_nraparitii)
		{
			cuv2[nrcuv2] = c[i].denumire;
			nrcuv2++;
		}
	}
	sortare(f, cuv2, nrcuv2, -1);
}

void bubblesort()
{
	CUVANT temp;
	int i, j;
	for (i = 0; i < 1001; i++)
	{
		for (j = 999; j >= i; j--)
		{
			if (c[j - 1].denumire > c[j].denumire)
			{
				temp = c[j - 1];
				c[j - 1] = c[j];
				c[j] = temp;
			}
		}
	}
}

void quicksort_nerecursiv()
{
	enum { m = 31 };
	int i, j;
	int stanga, dreapta;
	CUVANT temp;
	CUVANT x;
	int is;

	struct {
		int stanga, dreapta;
	}stiva[m];

	is = 0; stiva[0].stanga = 0; stiva[0].dreapta = 999;

	do {
		stanga = stiva[is].stanga; 
		dreapta = stiva[is].dreapta;
		is = is - 1;
		do
		{
			i = stanga; j = dreapta;
			x = c[(stanga + dreapta) / 2];
			do
			{
				while (c[i].denumire < c->denumire) i = i + 1;
				while (x.denumire < c[j].denumire) j = j - 1;
				if (i <= j)
				{
					temp = c[i]; c[i] = c[j]; c[j] = temp;
					i = i + 1; j = j - 1;
				}
			} while (!(i > j));
			if (i < dreapta)
			{ 
				is = is + 1; 
				stiva[is].stanga = i; 
				stiva[is].dreapta = dreapta;
			}
			dreapta = j; 
		} while (!(stanga >= dreapta)); 
	} while (!(is == -1));
}

void ordonarecresc_descresc() //sortam cuvintele in ordine crescatoare, cat si descrescatoare
{
	CUVANT temp;
	for (int i = 0; i < 999; i++)
	{
		for (int j = i + 1; j < 1000; j++)
		{
			if (c[i].total > c[j].total)
			{
				temp = c[i];
				c[i] = c[j];
				c[j] = temp;
			}
			else
			{
				if (c[i].total == c[j].total && strcmp(c[i].denumire, c[j].denumire) < 0)
				{
					temp = c[i];
					c[i] = c[j];
					c[j] = temp;
				}
			}
		}
	}
}

void afisare(FILE *f)
{
	for (int i = 0; i < 1000; i++)
	{
		fprintf(f, "%s %d %.2f\n", c[i].denumire, c[i].nr_aparitii, c[i].frecventa);
	}
}

int main(void)
{
	FILE* f = NULL;
	if ((f = fopen("output.txt", "w")) == NULL)
	{
		printf("fisierul nu poate fi prelucrat");
		return 0;
	}
	read();
	determinare_frecventa_nraparitii(f);
	bubblesort();
	quicksort_nerecursiv();
	ordonarecresc_descresc();
	afisare(f);
	if (fclose(f) != 0)
	{
		printf("nu s-a putut inchide fisierul");
		return 0;
	}
	return 0;
}